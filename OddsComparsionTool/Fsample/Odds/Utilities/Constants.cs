﻿namespace Odds.Utilities
{
    public static class Constants
    {
        public const string Live =
            @"https://line-01.ccf4ab51771cacd46d.com/live/updatesFromVersion/1589480771/en/?0.6877148933038781";

        public const string PreMatch = @"https://line-02.ccf4ab51771cacd46d.com/line/currentLine/en/?0.7882736278539362";

        public const char Plus = '+';
        public const char Minus = '-';
    }
}
