﻿using Odds.Models;
using Odds.Models.Totals;
using Odds.Utilities.Enums;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Odds.Utilities
{
    public class Helper
    {
        public static void FilterWithStartValue(Same param, char? startValue)
        {
            if (startValue.HasValue)
                param.Factors = param.Factors.Where(x => x.Pt.FirstOrDefault() == startValue).ToList();
            else
            {
                param.Factors = param.Factors
                    .Where(x => x.Pt.FirstOrDefault() != Constants.Plus || x.Pt.FirstOrDefault() != Constants.Minus)
                    .ToList();
            }
        }

        public static async Task<IEnumerable<Game>> FilterResultAsync(Same result)
        {
            return await Task.Run(() =>
            {
                IEnumerable<Game> query = (from factor in result.Factors
                                           join @event in result.Events on @factor.SportId equals @event.EventId
                                           where factor.Mark == (int)MatchProperty.FirstTeamWin
                                                 || factor.Mark == (int)MatchProperty.SecondTeamWin
                                                 || factor.Mark == (int)MatchProperty.X

                                           // where factor.Pt != null

                                           select new Game
                                           {
                                               GameId = @event.EventId,
                                               SportId = @event.SportId,
                                               FirstTeam = @event.FirstTeam,
                                               SecondTeam = @event.SecondTeam,
                                               Coefficients = new Coefficient
                                               {
                                                   FirstWin = factor.Mark == (int)MatchProperty.FirstTeamWin ? factor.Value : 0,
                                                   SecondWin = factor.Mark == (int)MatchProperty.SecondTeamWin ? factor.Value : 0,
                                                   X = factor.Mark == (int)MatchProperty.X ? factor.Value : 0
                                               }
                                           }).GroupBy(x => x.GameId).Select(g => new Game
                                           {
                                               GameId = g.Select(x => x.GameId).First(),
                                               FirstTeam = g.Select(x => x.FirstTeam).First(),
                                               SecondTeam = g.Select(x => x.SecondTeam).First(),
                                               SportId = g.Select(x => x.SportId).FirstOrDefault(),
                                               Coefficients = new Coefficient
                                               {
                                                   // TODO: optimize
                                                   FirstWin = g.Where(x => x.Coefficients.FirstWin != 0).Select(x => x.Coefficients.FirstWin)
                                                   .FirstOrDefault(),
                                                   SecondWin = g.Where(x => x.Coefficients.SecondWin != 0)
                                                   .Select(x => x.Coefficients.SecondWin).FirstOrDefault(),
                                                   X = g.Where(x => x.Coefficients.X != 0)
                                                   .Select(x => x.Coefficients.X).FirstOrDefault()
                                               }
                                           });
                return query;
            });
        }

        public static async Task<IEnumerable<Game>> GetTotals(Same result, SportType type)
        {

            return await Task.Run(() =>
            {
                IEnumerable<Game> query = (from factor in result.Factors
                                           join @event in result.Events on @factor.SportId equals @event.EventId
                                           where factor.Mark == (int)TotalProperty.Over
                                                 || factor.Mark == (int)TotalProperty.Under
                                           select new Game
                                           {
                                               GameId = @event.EventId,
                                               SportId = @event.SportId,
                                               Total = new Total
                                               {
                                                   Over = factor.Mark == (int)TotalProperty.Over ? factor.Value : 0,
                                                   Under = factor.Mark == (int)TotalProperty.Under ? factor.Value : 0,
                                                   // Totale = factor.Mark == (int)TotalProperty. change here
                                               }
                                           }).GroupBy(x => x.GameId).Select(g => new Game
                                           {
                                               SportId = g.Select(x => x.SportId).First(),
                                               GameId = g.Select(x => x.GameId).First(),
                                               Total = new Total
                                               {
                                                   Under = g.Where(x => x.Total.Under != 0).Select(x => x.Total.Under).FirstOrDefault(),
                                                   Over = g.Where(x => x.Total.Over != 0).Select(x => x.Total.Over).FirstOrDefault(),
                                                   Totale = g.Where(x => x.Total.Totale != 0).Select(x => x.Total.Totale).FirstOrDefault()
                                               },
                                           });

                var test = from game in query
                           join sport in result.Sports on game.SportId equals sport.Id
                           where sport.ParentId == (int)type
                           select new Game
                           {
                               ParentId = (int)type,
                               SportId = sport.Id,
                               Total = game.Total,
                               GameId = game.GameId
                           };
                return test;
            });
        }
    }
}
