﻿using System;
using System.Linq;
using Newtonsoft.Json;
using Odds.Models;
using Odds.Utilities;
using System.Net.Http;
using System.Threading.Tasks;

namespace Odds.Services
{
    public class HttpService : IHttpService
    {
        private HttpClient _client;
//        private Same result;
        public HttpService(HttpClient client)
        {
            _client = client;
        }

        public async Task<Same> GetLiveMatchesAsync()
        {
            var response = await _client.GetAsync(Constants.Live);
            var content = await response.Content.ReadAsStringAsync();

            var result = JsonConvert.DeserializeObject<Same>(content);
            if (ReferenceEquals(result, null))
            {
                await GetLiveMatchesAsync();
            }
            // Filter(result);
            return result;
        }


        public async Task<Same> GetPreMatchesAsync()
        {
            var response = await _client.GetAsync(Constants.PreMatch);
            var content = await response.Content.ReadAsStringAsync();
            var result = JsonConvert.DeserializeObject<Same>(content);
            // Filter(result);
            return result;
        }

        private void Filter(Same result)
        {
            // if method executed the result is always null
            // see line 40
            result.Factors = result.Factors.Where(x => x.Pt != null).ToList();

            Console.WriteLine();
        }
    }
}
