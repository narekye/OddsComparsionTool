﻿using Newtonsoft.Json;

namespace Odds.Models
{
    public class Factor
    {
        [JsonProperty("e")]
        public int SportId { get; set; }
        [JsonProperty("f")]
        public int Mark { get; set; }
        [JsonProperty("v")]
        public decimal Value { get; set; }
        [JsonProperty("pt")]
        public string Pt { get; set; }
        [JsonProperty("isLive")]
        public bool IsLiveNow { get; set; }
    }
}
