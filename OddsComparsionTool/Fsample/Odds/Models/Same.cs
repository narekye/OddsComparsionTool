﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace Odds.Models
{
    public class Same
    {
        [JsonProperty("sports")]
        public List<Sport> Sports { get; set; }
        [JsonProperty("customFactors")]
        public List<Factor> Factors { get; set; }
        [JsonProperty("events")]
        public List<Event> Events { get; set; }
    }
}
