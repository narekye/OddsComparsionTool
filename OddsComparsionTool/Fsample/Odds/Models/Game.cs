﻿using Newtonsoft.Json;
using Odds.Models.Totals;

namespace Odds.Models
{
    public class Game
    {
        public int ParentId { get; set; }
        public int SportId { get; set; }
        public int GameId { get; set; }
        public string FirstTeam { get; set; }
        public string SecondTeam { get; set; }
        public Coefficient Coefficients { get; set; }
        [JsonProperty("v")]
        public Total Total { get; set; }
    }
}
