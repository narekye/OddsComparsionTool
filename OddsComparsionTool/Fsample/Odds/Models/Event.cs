﻿using Newtonsoft.Json;

namespace Odds.Models
{
    public class Event
    {
        [JsonProperty("id")]
        public int EventId { get; set; }
        [JsonProperty("sportId")]
        public int SportId { get; set; }
        [JsonProperty("team1")]
        public string FirstTeam { get; set; }
        [JsonProperty("team2")]
        public string SecondTeam { get; set; }
        [JsonProperty("startTime")]
        public string StartTime { get; set; }
    }
}
