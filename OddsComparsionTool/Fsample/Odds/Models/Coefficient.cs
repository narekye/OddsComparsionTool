﻿namespace Odds.Models
{
    public class Coefficient
    {
        public decimal FirstWin { get; set; }
        public decimal SecondWin { get; set; }
        public decimal X { get; set; }
    }
}
