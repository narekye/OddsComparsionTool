﻿using Newtonsoft.Json;

namespace Odds.Models
{
    public class Sport
    {
        [JsonProperty("id")]
        public int Id { get; set; }
        [JsonProperty("parentId")]
        public int? ParentId { get; set; }
        [JsonProperty("kind")]
        public string Kind { get; set; }
        [JsonProperty("sortOrder")]
        public string SortOrder { get; set; }
        [JsonProperty("name")]
        public string Name { get; set; }
    }
}
