﻿using Newtonsoft.Json;

namespace Odds.Models.Totals
{
    public class Total
    {
        [JsonProperty("v")]
        public decimal Totale { get; set; }
        public decimal Over { get; set; }
        public decimal Under { get; set; }
    }
}
