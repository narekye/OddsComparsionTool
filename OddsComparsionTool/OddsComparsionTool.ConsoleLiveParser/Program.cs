﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace OddsComparsionTool.ConsoleLiveParser
{
    class Program
    {
        private static string url = "https://e1-api.aws.kambicdn.com/offering/api/v2/888/event/live/open.json?lang=ru_RU&market=ZZ&client_id=2&channel_id=1&ncid=1502504837513";
        private static Uri uri = new Uri(url);
        static void Main(string[] args)
        {
            HttpClient client = new HttpClient();
            Console.WriteLine("start");
            var response = client.GetAsync(uri).Result;
            if (response.IsSuccessStatusCode)
            {
                var result = JsonConvert.DeserializeObject(response.Content.ReadAsStringAsync().Result);
                Console.WriteLine(result);
            }
            Console.Read();
        }
    }
}
